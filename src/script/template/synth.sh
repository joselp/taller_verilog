#!/bin/bash

    READ=$1
    TOP=${READ/.v/}
    TOP=${TOP/'src/cond/'/}
    WRITE=$2

    # se crea el directorio synt si no existe.
    mkdir -p ./src/synth

    # se crea el directorio dot si no existe.
    mkdir -p ./dot

    mkdir -p ./log

    # se realiza la síntesis.
	yosys -p "

        read_verilog $READ
    
        hierarchy -check -top $TOP

        proc; opt; fsm; opt; memory; opt

        SYNTH_TYPE

        dfflibmap -liberty ./lib/LIBRARY.lib

        abc -liberty ./lib/LIBRARY.lib; opt

        stat -liberty ./lib/LIBRARY.lib

        clean

        show -format ps -prefix ./dot/$TOP
    
        write_verilog -attr2comment $WRITE
    
    " > ./log/LIBRARY_$TOP.log
    # se cambia el nombre del módulo
	sed -i "s/_c/_s/g" $WRITE

    # se agrega la biblioteca de componentes.
    sed -i '1a `include "LIBRARY.v"' $WRITE

