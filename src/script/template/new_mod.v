/***
 **
 * MODULE: MODULE_NAME
 *   Module description.
 *
 *      => Inputs
 *          #name  : description.
 *
 *      <= Outputs
 *          #name  : description.
 *
 *	* Nodos Internos
 *          #name  : description.
 *
 *	* Parametros
 *	    #name  : description.
 **
 ***/

`ifndef MODULE_NAME_C
`define MODULE_NAME_C

    // Includes

    module MODULE_NAME #(
        // Parameters
    )(
        // Inputs & Outputs
    );

    
    `ifdef FORMAL
        // Formal Verification
    `endif // FORMAL

    endmodule // MODULE_NAME

`endif // MODULE_NAME_C

