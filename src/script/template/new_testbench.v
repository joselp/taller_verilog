/***
 **
 * Testbench name: TB_NAME
 **
 ***/

`include "MODULE_NAME.v"
`include "MODULE_NAME_synth.v"
`include "tester.v"

module testbench();

	// Parameters
	TB_PARAM

	// Wires
	TB_WIRES

	// Behavioral model instance
	COND_INST

	// Structural model instance
	SYNTH_INST

	// Tester instance
  	TESTER_INST

endmodule // TESTBENCH
