
`timescale 1ns/1ns

module t_MODULE_NAME #(
  PARAMETER
)(
  INOUT
);
  
  initial begin
	  $dumpfile("./testbench/TB_NAME/sim.vcd"); 
	  $dumpvars;
    
    $finish;
  end

endmodule // t_MODULE_NAME

