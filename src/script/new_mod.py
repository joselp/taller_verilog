#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from Util import *

if __name__ == "__main__":    # Patrones

    # Creación de directorio src
    src_path  = "./src"
    Util.create_dir(src_path)

    mod_name   = input("Nombre del módulo: ")
    mod_name   = mod_name.lower()
    
    out_path  = "./src/" + mod_name + ".v"
    temp_path  = "./script/template/new_mod.v"

    # Creación del módulo
    if( not os.path.isfile(out_path) ):
    	Util.create_module(mod_name,temp_path,out_path)
    else:
    	print("[ERROR] El módulo ya existe")

