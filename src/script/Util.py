#! /usr/bin/python3
# coding=utf-8

import os, re                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
from pathlib import Path
from dataclasses import dataclass
from typing import List

re_parameter = re.compile(r".*parameter\s+([\w_]+)\s+=\s+(\d+),?\s+")
re_input = re.compile(r".*(input)\s+(reg|wire)?\s*(\[\s*[\w\-_]+\s*:\s*[\w_]+\s*\])?\s+(\w+),?")
re_output = re.compile(r".*(output)\s+(reg|wire)?\s*(\[\s*[\w\-_]+\s*:\s*[\w_]+\s*\])?\s+(\w+),?")

@dataclass
class Parameter:
    name: str
    value: str

    def to_string (self):
        return "." + self.name + "(" + self.value + ")"

@dataclass
class Node:
    name: str
    wtype: str
    ntype: str 
    size: str

    def to_string (self):
        return  self.ntype  +'\t'+  self.wtype + '\t' + self.size + '\t' + self.name

@dataclass
class Module:
    name: str
    parameters: List[Parameter]
    inputs: List[Node]
    outputs: List[Node]

@dataclass
class Connection:
    intern: Node
    extern: Node

    def to_string(self):
        return "." + self.intern.name + "(" + self.extern.name + ")"

@dataclass
class Instance:
    instance_name: str
    module_name: str
    parameters: List[Parameter]
    connections: List[Connection]

    def _parameters_to_string(self):
        string = ""
        for parameter in self.parameters:
            string += parameter.to_string() + ',\n\t'
        return string[:-3]

    def _connections_to_string(self):
        string = ""
        for connection in self.connections:
            string += connection.to_string() + ',\n\t'
        return string[:-3]

    def to_string(self, type=False):
        template = "MODULE_N NAME (\n\tCONNECTIONS\n);"
        if (type):
            template = "MODULE_N #(\n\tPARAMETERS\n) NAME (\n\tCONNECTIONS\n);"
        parameters = self._parameters_to_string()
        conections = self._connections_to_string()
        template = re.sub(r"MODULE_N", self.module_name, template)
        template = re.sub(r"NAME", self.instance_name, template)
        template = re.sub(r"PARAMETERS", parameters, template)
        template = re.sub(r"CONNECTIONS", conections, template)
        return template

class Util:

    @staticmethod    
    def to_parameters( parameters ):
        result = []
        for parameter in parameters:
            name = parameter[0]
            value = parameter[1]
            result.append(Parameter(name, value))
        return result

    @staticmethod
    def to_node ( nodes ):
        result = []
        for node in nodes:
            ntype = node[0]
            wtype = node[1] if node[1]== 'reg' else 'wire' 
            size = node[2]
            name = node[3]
            result.append(Node(name, wtype, ntype, size))
        return result

    @staticmethod
    def to_conections ( interns, externs ):
        conections = []
        if ( len(interns) != len(externs) ):
            return conections
        for i in range(len(interns)):
            conections.append(Connection(interns[i], externs[i]))
        return conections;       

    # Funcion para crear directorios
    def create_dir(path):
        try:
            os.stat(path)
        except:
            os.mkdir(path)

    def create_module(module_name, temp_path, out_path):
        # Factory module
        txt = Path(temp_path).read_text()
        txt = re.sub(r'MODULE_NAME_C', module_name.upper()+"_C", txt)
        txt = re.sub(r'MODULE_NAME', module_name, txt)
        # Write module in file
        out_file  = open(out_path, "w")
        out_file.write(txt)
        out_file.close()

    @staticmethod
    def read_module(path):
        str_file = Path(path).read_text()
        name = re.search(r'/([\w_]+).v',path).group(1)
        parameters = Util.to_parameters(re.findall(re_parameter,str_file))
        inputs = Util.to_node(re.findall(re_input,str_file))
        outputs = Util.to_node(re.findall(re_output,str_file))
        return Module(name, parameters, inputs, outputs)


