#! /usr/bin/python3
# coding=utf-8

from os import listdir
import os, sys, re


def get_options(rute, extension=""):
	files   = listdir(rute)
	files.sort()
	options = []
	for i in range(len(files)):
		file      = files[i]
		file_name = re.sub(rf"{extension}", '', file)
		options.append([file_name,file])
	
	return options

def print_menu(header,options):
	print(header)
	for i in range(len(options)):
		option_line = "\t"+ str(i) +") " + options[i][0]
		print(option_line)

def choose_option(options):
	choosen = input()
	try:
		choosen = int(choosen)
		if(choosen >= 0 and choosen < len(options)):
			return options[choosen]
		else:
			return ["ERROR","ERROR"]
	except:
		return ["ERROR","ERROR"]

def menu(rute, header, extension=""):
	options = get_options(rute, extension)
	print_menu(header,options)
	return choose_option(options) 

if __name__ == "__main__":
	
	rute      = "./"	
	header    = "Escoja su opción:"
	extension = ".py"

	print(menu(rute, header, extension))
