/***
 **
 * MODULE: nibble_selector_c v(0.0.0)
 *   Module description.
 *
 *      => Inputs
 *          #name   : description.
 *
 *      <= Outputs
 *          #name   : description.
 *
 *	* Nodos Internos
 *          #name   : description.
 *
 *	* Parametros
 *	         #name   : description.
 **
 ***/

`ifndef NIBBLE_SELECTOR
`define NIBBLE_SELECTOR

// Includes

module nibble_selector_c #(
	parameter   WIDTH_IN        = 32,
	parameter   WIDTH_N         = 4, 
	parameter   WIDTH_SEL       = 3 
)(
	// Inputs & Outputs
	input 		               clk,
	input 		               resetL,
	input 		               in_valid_A,
	input 		               in_valid_B,
	input [WIDTH_IN-1:0]       in_dt_A,
	input [WIDTH_IN-1:0]       in_dt_B, 
	input [WIDTH_SEL-1:0]      sel_nibble_A,
	input [WIDTH_SEL-1:0]      sel_nibble_B,
	input 		               sel_nibble_AB,

	output reg [WIDTH_N-1:0]   out_nibble,
	output reg 	               out_valid
);

   function      [WIDTH_N-1:0]     get_n_selected;
      input [WIDTH_SEL-1:0] 	n_selector;
      input [WIDTH_IN-1:0] 	in_dt; 
      begin  
         get_n_selected = in_dt[4*n_selector+:4];
      end
   endfunction
   
   function      [WIDTH_N-1:0]     get_n_A_or_B_selected;
      input     [WIDTH_N-1:0]    n_A_selected;
      input [WIDTH_N-1:0] 	      n_B_selected;
      input                      sel_n_AB;
      begin  
         get_n_A_or_B_selected = (sel_n_AB)? n_B_selected : n_A_selected;
      end
   endfunction

   wire [WIDTH_N-1:0] nibble_A_selected, nibble_B_selected, nibble_A_or_B_selected;
   wire 	      valid_aux;

   assign nibble_A_selected		=  get_n_selected(sel_nibble_A,in_dt_A);
   assign nibble_B_selected		=  get_n_selected(sel_nibble_B,in_dt_B);
   assign nibble_A_or_B_selected	=  get_n_A_or_B_selected(nibble_A_selected,nibble_B_selected,sel_nibble_AB);
   assign valid_out_aux			   =  (sel_nibble_AB)? in_valid_B : in_valid_A;
   
   always @(posedge clk) begin
      if(~resetL) begin
	      out_nibble <= 0;
	      out_valid  <= 0;
      end else begin
	      out_valid  <= valid_out_aux;
	      if(valid_out_aux) begin
	         out_nibble <= nibble_A_or_B_selected;
	      end
      end
   end


endmodule // nibble_selector

`endif // NIBBLE_SELECTOR

